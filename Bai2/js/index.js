let dssp = [];
const BASE_URL = "https://62f8b754e0564480352bf3de.mockapi.io";

//LẤY THÔNG TIN RA BẢNG
let renderDssp = () => {
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then((res) => {
      dssp = res.data;
      renderTable(dssp);
    })
    .catch((err) => {
      console.log(err);
    });
};
renderDssp();

//THÊM SP
let themSanPham = () => {
  var dataForm = layThongTinTuForm();
  let sp = new sanPham(
    dataForm.name,
    dataForm.price,
    dataForm.screen,
    dataForm.backCamera,
    dataForm.frontCamera,
    dataForm.img,
    dataForm.desc,
    dataForm.type,
    dataForm.id
  );
  //name
  let isValid = validator.kiemTraChuoiRong(dataForm.name, "tbTenSp");
  //price
  isValid =
    isValid & validator.kiemTraChuoiRong(dataForm.price, "tbPrice") &&
    validator.kiemTraChuoiSo(dataForm.price, "tbPrice");
  //screen
  isValid = isValid & validator.kiemTraChuoiRong(dataForm.screen, "tbScreen");

  //backcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataForm.backCamera, "tbBackCamera");

  //frontcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataForm.frontCamera, "tbFrontCamera");

  //img
  isValid = isValid & validator.kiemTraChuoiRong(dataForm.img, "tbImg");

  //desc
  isValid = isValid & validator.kiemTraChuoiRong(dataForm.desc, "tbMota");
  //type
  isValid = isValid & validator.kiemTraLoai(dataForm.type, "tbTypePhone");

  if ((isValid == false)) {
    return;
  }
  axios({
    url: `${BASE_URL}/phone`,
    method: "POST",
    data: sp,
  })
    .then((res) => {
      console.log("res: ", res);
      renderTable();
      
      $("#myModal").modal("hide");
      alert("Bạn đã update thành công sản phẩm")
      location.reload();
    })
    .catch((err) => {
      console.log(err);
    });
};

// XÓA SP
let xoaSanPham = (id) => {
  $("#delete-modal").modal("show");
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderDssp();
      console.log("res: ", res);
    
      location.reload();
    })
    .catch((err) => {
      console.log(err);
    });
};

// SỬA SP
let suaSanPham = (id) => {
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: "GET",
  })
    .then((res) => {
      hienThiThongTin(res.data);
      console.log("res: ", res);
    })
    .catch((err) => {
      console.log(err);
    });
};

//UPDATE SP
let capNhatSanPham = () => {
  let dataSp = layThongTinTuForm();
  console.log("dataSp: ", dataSp);

  //name
  let isValid = validator.kiemTraChuoiRong(dataSp.name, "tbTenSp");
  //price
  isValid =
    isValid & validator.kiemTraChuoiRong(dataSp.price, "tbPrice") &&
    validator.kiemTraChuoiSo(dataSp.price, "tbPrice");
  //screen
  isValid = isValid & validator.kiemTraChuoiRong(dataSp.screen, "tbScreen");

  //backcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataSp.backCamera, "tbBackCamera");

  //frontcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataSp.frontCamera, "tbFrontCamera");

  //img
  isValid = isValid & validator.kiemTraChuoiRong(dataSp.img, "tbImg");

  //desc
  isValid = isValid & validator.kiemTraChuoiRong(dataSp.desc, "tbMota");
  //type
  isValid = isValid & validator.kiemTraLoai(dataSp.type, "tbTypePhone");

  if ((isValid == false)) {
    return;
  }

  axios({
    url: `${BASE_URL}/phone/${dataSp.id}`,
    method: "PUT",
    data: dataSp,
  })
    .then((res) => {
      console.log("res: ", res);
      renderDssp();
      document.getElementById("myform").reset();
      location.reload();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

document.getElementById("btnThem").onclick = () => {
  document.getElementById("myform").reset();
}